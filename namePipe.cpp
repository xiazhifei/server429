#include "stdafx.h"
#include "namePipe.h"

/*
参考资料：
http://blog.csdn.net/typecool/article/details/6230475
http://blog.csdn.net/QHH_QHH/article/details/49183401
*/

#define PIPE_BUF_LEN	128

//创建命名管道(Server)
HANDLE createNP(char* name)
{	
	return CreateNamedPipe(	name,											// pointer to pipe name
							PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED | WRITE_DAC,		// pipe open mode	双向 重叠(异步)
							PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_NOWAIT,// pipe-specific modes
							PIPE_UNLIMITED_INSTANCES,						// maximum number of instances
							PIPE_BUF_LEN,									// output buffer size, in bytes
							PIPE_BUF_LEN,									// input buffer size, in bytes
							100,											// time-out time, in milliseconds
							NULL);											// pointer to security attributes
}


//打开命名管道(Client)
HANDLE openNP(char* name)
{      
    return CreateFile(	name,												// pointer to name of the file
						GENERIC_READ|GENERIC_WRITE,							// access (read-write) mode
						0,													// share mode
						NULL,												// pointer to security attributes
						OPEN_EXISTING,										// how to create
						FILE_FLAG_OVERLAPPED,								// file attributes
						NULL);												// handle to file with attributes to copy
}

//读命名管道
DWORD readNP(HANDLE hNP, char* buf)
{   
	DWORD  dwRead = NULL;  
	//OVERLAPPED overlap;
	//memset(&overlap, 0, sizeof(overlap));
	if (PeekNamedPipe(hNP,buf,PIPE_BUF_LEN,&dwRead,0,0))
		if(dwRead)
			ReadFile(hNP, buf, dwRead, &dwRead, NULL);
			//ReadFile(hNP, buf, sizeof(buf), &dwRead, &overlap);
	return dwRead;	
}

//写命名管道
DWORD writeNP(HANDLE hNP, char* buf)
{
	DWORD dwWrite = NULL;    
	WriteFile(hNP, buf, strlen(buf), &dwWrite, NULL);  
	   return dwWrite;
}

