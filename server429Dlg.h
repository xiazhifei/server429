// server429Dlg.h : header file
//

#if !defined(AFX_SERVER429DLG_H__E52AB3A5_A482_45B6_A861_2B2A022ED034__INCLUDED_)
#define AFX_SERVER429DLG_H__E52AB3A5_A482_45B6_A861_2B2A022ED034__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CServer429Dlg dialog

class CServer429Dlg : public CDialog
{
// Construction
public:
	CServer429Dlg(CWnd* pParent = NULL);	// standard constructor

	//����ͼ��
	LRESULT OnShowTask(WPARAM wParam,LPARAM lParam) ;
	void ToTray();
	void DeleteTray();

// Dialog Data
	//{{AFX_DATA(CServer429Dlg)
	enum { IDD = IDD_SERVER429_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CServer429Dlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CServer429Dlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();	
	afx_msg void OnDestroy();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	virtual void OnOK();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERVER429DLG_H__E52AB3A5_A482_45B6_A861_2B2A022ED034__INCLUDED_)
