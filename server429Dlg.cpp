// server429Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "server429.h"
#include "server429Dlg.h"
#include "namePipe.h"
#include  "mmsystem.h"
#pragma comment(lib,"winmm.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define WM_SHOWTASK (WM_USER +1)

#define PIPE_BUF_LEN 128

void CALLBACK TimeProc429(UINT uID,UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2);

//全局变量   

HANDLE h429[8];
CListCtrl* plst =NULL;

/////////////////////////////////////////////////////////////////////////////
// CServer429Dlg dialog

CServer429Dlg::CServer429Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CServer429Dlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CServer429Dlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CServer429Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CServer429Dlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CServer429Dlg, CDialog)
	//{{AFX_MSG_MAP(CServer429Dlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_WM_SYSCOMMAND()
	ON_MESSAGE(WM_SHOWTASK,OnShowTask)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CServer429Dlg message handlers

BOOL CServer429Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	int i = 0;
	CString str;
	char buf[128],buf2[128];

	//listctrl
	plst = (CListCtrl*)GetDlgItem(IDC_LIST);
	plst->SetExtendedStyle(plst->GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES );
	plst->InsertColumn( 0, _T("通道"), LVCFMT_LEFT, 40 );
	plst->InsertColumn( 1, _T("名称"), LVCFMT_LEFT, 60 );	//40
	plst->InsertColumn( 2, _T("配置"), LVCFMT_LEFT, 60);	//50
	plst->InsertColumn( 3, _T("状态"), LVCFMT_LEFT, 80);	//110
	for(i=0;i<8;i++)
	{
		//通道
		str.Format("%d",i+1);
		plst->InsertItem(i, str);
		//名称
		sprintf(buf,"ch%d",i+1);
		GetPrivateProfileString(buf,"name",NULL,buf2,sizeof(buf2)-1,"./config.ini");
		plst->SetItemText(i,1,buf2);
		//配置
		GetPrivateProfileString(buf,"set",NULL,buf2,sizeof(buf2)-1,"./config.ini");
		plst->SetItemText(i,2,buf2);
	}

	//定时器1，建立/重建匿名管道
	SetTimer(1,500,NULL);
	//定时器2, 开thread后台处理数据
	timeSetEvent(TIME_PERIODIC,0,TimeProc429,NULL,TIME_PERIODIC);		//1ms
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CServer429Dlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;
		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CServer429Dlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CServer429Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	CDialog::OnSysCommand(nID, lParam);
	if(nID==SC_MINIMIZE) ToTray(); //最小化到托盘的函数
}

void CServer429Dlg::OnDestroy() 
{
	CDialog::OnDestroy();	
	//删除托盘图标
	DeleteTray();	
}

//显示托盘图标
void CServer429Dlg::ToTray()
{
	NOTIFYICONDATA nid;
	nid.cbSize=(DWORD)sizeof(NOTIFYICONDATA);
	nid.hWnd=this->m_hWnd; nid.uID=IDR_MAINFRAME;
	nid.uFlags=NIF_ICON|NIF_MESSAGE|NIF_TIP ;
	nid.uCallbackMessage=WM_SHOWTASK;//自定义的消息名称
	nid.hIcon=LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDR_MAINFRAME));
	strcpy(nid.szTip,"Smart Test System"); //信息提示条
	Shell_NotifyIcon(NIM_ADD,&nid); //在托盘区添加图标
	ShowWindow(SW_HIDE); //隐藏主窗口
}

//删除托盘图标
void CServer429Dlg::DeleteTray()
{
	NOTIFYICONDATA nid;
	nid.cbSize=(DWORD)sizeof(NOTIFYICONDATA);
	nid.hWnd=this->m_hWnd;
	nid.uID=IDR_MAINFRAME;
	nid.uFlags=NIF_ICON|NIF_MESSAGE|NIF_TIP ;
	nid.uCallbackMessage=WM_SHOWTASK;//自定义的消息名称
	nid.hIcon=LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDR_MAINFRAME));
	strcpy(nid.szTip,"Smart Test System"); //信息提示条
	Shell_NotifyIcon(NIM_DELETE,&nid); //在托盘区删除图标
}

LRESULT CServer429Dlg::OnShowTask(WPARAM wParam,LPARAM lParam)
{
	if(wParam!=IDR_MAINFRAME)
		return 1;
	switch(lParam)
	{
	case WM_RBUTTONUP:			//右键
		{ 
			LPPOINT lpoint=new tagPOINT;
			::GetCursorPos(lpoint);		//鼠标位置
			CMenu menu;
			menu.CreatePopupMenu();		//弹出式菜单	
			//menu.AppendMenu(MF_STRING,IDOPEN,_T("OPEN"));					//OPEN
			menu.AppendMenu(MF_STRING,IDOK,_T("SHOW"));						//SHOW			
			//menu.AppendMenu(MF_STRING,IDHELP,_T("HELP"));					//HELP
			menu.AppendMenu(MF_STRING,IDCANCEL,_T("EXIT"));					//EXIT，也可以WM_DESTROY
			menu.TrackPopupMenu(TPM_LEFTALIGN,lpoint->x,lpoint->y,this);	//菜单位置	
			HMENU hmenu=menu.Detach();										//资源回收
			menu.DestroyMenu();
			delete lpoint;
		} 
		break;
	case WM_LBUTTONDBLCLK:		//双击左键
		{ 
			ShowWindow(SW_SHOW);		//显示主窗口
			//ShowWindow(SW_SHOWNORMAL);  // 显示主窗口
			DeleteTray();
		} 
		break;
	default: 
		break;
	}
	return 0;
}

void CServer429Dlg::OnOK() 
{
	// TODO: Add extra validation here
	ShowWindow(SW_SHOWNORMAL);  // 显示主窗口	
	//CDialog::OnOK();
}

//收发429数据，此处只是简单地将收到的数据再发出去
void CALLBACK TimeProc429(UINT uID,UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2)
{	
	static char buf[1024];
	static DWORD dwRead = NULL; 
	static DWORD dwWrite = NULL;

	for(int i=0;i<8;i++)
	{
		//read
		ZeroMemory(buf,sizeof(buf));		
		if (PeekNamedPipe(h429[i],buf,PIPE_BUF_LEN,&dwRead,0,0))
			if(dwRead)
				ReadFile(h429[i], buf, dwRead, &dwRead, NULL);
		//write			    
			WriteFile(h429[i], buf, strlen(buf), &dwWrite, NULL); 
	}
	
}

void CServer429Dlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	int i = 0;
	CString str;
	char buf[128],buf2[128],buf3[128];
	if(1 == nIDEvent)	//namepipe	
	{		
		for(i=0;i<8;i++)
		{	
			ConnectNamedPipe(h429[i],NULL);
			switch(GetLastError())
			{
			case ERROR_NO_DATA:			//The pipe is being closed			  此处不用break，和ERROR_INVALID_HANDLE一起的
				DisconnectNamedPipe(h429[i]);
				CloseHandle(h429[i]);
			case ERROR_INVALID_HANDLE:	//The handle is invalid
				sprintf(buf,"ch%d",i+1);
				GetPrivateProfileString(buf,"name",NULL,buf2,sizeof(buf2)-1,"./config.ini");
				sprintf(buf3,"\\\\.\\pipe\\%s",buf2);
				h429[i] = CreateNamedPipe(buf3,								// pointer to pipe name
					PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED | WRITE_DAC,		// pipe open mode	双向 重叠(异步)
					PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_NOWAIT,			// pipe-specific modes
					1,//PIPE_UNLIMITED_INSTANCES,						// maximum number of instances
					PIPE_BUF_LEN,									// output buffer size, in bytes
					PIPE_BUF_LEN,									// input buffer size, in bytes
					100,											// time-out time, in milliseconds
					NULL);											// pointer to security attributes
				break;
			case ERROR_PIPE_LISTENING:	//Waiting for a process to open the other end of the pipe
				plst->SetItemText(i,3,"等待");
				break;
			case ERROR_PIPE_CONNECTED:	//There is a process on other end of the pipe
				plst->SetItemText(i,3,"已连接");
				break;
			default:				
				break;
			}				
		}
	}	
	CDialog::OnTimer(nIDEvent);
}
