// server429.h : main header file for the SERVER429 application
//

#if !defined(AFX_SERVER429_H__01C045E4_CCD6_48F4_8B7B_27C240C12858__INCLUDED_)
#define AFX_SERVER429_H__01C045E4_CCD6_48F4_8B7B_27C240C12858__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CServer429App:
// See server429.cpp for the implementation of this class
//

class CServer429App : public CWinApp
{
public:
	CServer429App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CServer429App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CServer429App)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERVER429_H__01C045E4_CCD6_48F4_8B7B_27C240C12858__INCLUDED_)
